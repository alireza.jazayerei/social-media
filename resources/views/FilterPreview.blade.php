<div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
    <div class="card">
        <div class="card-image">
            <figure class="filter-{{$filter}}">

                <img class="img-responsive img-upload" @isset($post) src="{{asset("storage/users/".$post->media->src)}}" @endif alt="Image"/>
            </figure>

        </div>
        <div class="card-header">
            <span>{{$filter??"Normal"}}</span>
            <input type="radio" name="filter" value="{{$filter}}" @if(isset($post)){{$post->filter===$filter?'checked':''}}@endif>
        </div>
    </div>
</div>

