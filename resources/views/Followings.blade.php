@include("Layouts.Header",["section"=>"profile"])

<section class="followers">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-8 col-lg-offset-2">

                <div class="followers-box full-width">
                    <div class="followers-list">
                        @foreach($followings as $following)
                            <div class="followers-body">
                                <img class="img-responsive img-circle"
                                     src="{{asset("storage/users/".$following->media->src)}}" alt="">
                                <div class="name-box">
                                    <h4>{{$following->name}}</h4>
                                    <a href="{{route("profile.show",["user"=>$following])}}">
                                        <span>{{"@".$following->username}}</span>
                                    </a>

                                </div><!--/ name-box -->

                                <form id="{{$following->id}}" action="{{route("follow.store",["user"=>$following])}}" method="post">
                                    @csrf

                                </form>

                                <span><a href="{{route("follow.store",["user"=>$following])}}"
                                         class="kafe-btn kafe-btn-mint-small" onclick="event.preventDefault();
                                        document.getElementById('{{$following->id}}').submit();">Unfollow</a></span>
                            </div><!--/ followers-body -->
                        @endforeach

                    </div><!--suggestions-list end-->
                </div>

            </div>

        </div><!--/ row-->
    </div><!--/ container -->
</section>

@include("Layouts.Footer")
