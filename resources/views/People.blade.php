@include("Layouts.Header",["section"=>""])

<section class="user-profile">

    <br><br><br>
    <br><br>
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <div class="post-content">
                    <div class="author-post text-center">

                        <div class="image-upload">
                            <label for="file-input">
                                <img class="img-fluid img-circle"
                                     src="{{asset("storage/users/".$user->media->src)}}" alt="Image">
                            </label>
                        </div>

                    </div><!-- /author -->
                </div><!-- /.post-content -->
            </div><!-- /col-sm-12 -->

        </div><!--/ row-->
    </div><!--/ container -->
</section><!--/ profile -->

<!-- ==============================================
User Profile Section
=============================================== -->

<section class="details">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <div class="details-box row">
                    <div class="col-lg-9">
                        <div class="content-box">
                            <h4>{{$user->name}}</h4>
                            <p>{{$user->bio}}</p>
                            <small><span>{{$user->website}}</span></small>

                        </div><!--/ media -->
                    </div>
                    <div class="col-lg-3">
                        <div class="follow-box mb-3">
                            <form action="{{route("follow.store",["user"=>$user])}}" method="post">
                                @csrf
                                @if($followingStatus || $requestedStatus)
                                    @method("DELETE")
                                @endif
                                <button class="kafe-btn kafe-btn-mint">
                                    @if($requestedStatus)
                                        Requested
                                    @elseif($followingStatus)
                                        Unfollow
                                    @else
                                        Follow
                                    @endif
                                </button>
                            </form>

                            <br><br>
                            <a href="" class="kafe-btn kafe-btn-danger">Send Message</a>
                            <br><br>
                        </div>
                    </div><!--/ dropdown -->
                </div>
            </div><!--/ details-box -->

        </div>
    </div>
</section><!--/ profile -->

<!-- ==============================================
Home Menu Section
=============================================== -->
<section class="home-menu">
    <div class="container">
        <div class="row">
            <div class="menu-category">
                <ul class="menu">
                    <li class="current-menu-item">Posts <span>{{$user->posts_count}}</span></li>
                    <li class="current-menu-item">
                        @can("view",[$user,$followingStatus])
                            <a href="{{route("follower.show",["user"=>$user])}}">
                                @endcan
                                Followers <span>{{$user->followers_count}}</span>
                                @can("view",[$user,$followingStatus])
                            </a>
                        @endcan
                    </li>
                    <li>
                        @can("view",[$user,$followingStatus])
                            <a href="{{route("following.show",["user"=>$user])}}">
                                @endcan
                                Following
                                <span>{{$user->followings_count}}</span>
                                @can("view",[$user,$followingStatus])
                            </a>
                        @endcan
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- ==============================================
News Feed Section
=============================================== -->
@can("view",[$user,$followingStatus])
    <section class="newsfeed">
        <div class="container">
            <div class="row">
                @foreach($user->posts as $post)
                    <div class="col-lg-4">
                        <a href="#{{$post->id}}" data-toggle="modal">
                            <div class="filter-{{$post->filter}}">
                                <div class="explorebox"
                                     style="background: linear-gradient( rgba(34,34,34,0.2), rgba(34,34,34,0.2)), url({{asset("storage/users/".$post->media->src)}}) no-repeat;
                                         background-size: cover;
                                         background-position: center center;
                                         -webkit-background-size: cover;
                                         -moz-background-size: cover;
                                         -o-background-size: cover;">
                                    <div class="explore-top">
                                        <div class="explore-like"><i class="fa fa-heart"></i>
                                            <span>{{$post->likes_count}}</span></div>
                                        <div class="explore-circle pull-right"><i class="far fa-bookmark"></i></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div><!--/ container -->
    </section><!--/ newsfeed -->
@endcan
@cannot("view",[$user,$followingStatus])
    <section class="newsfeed">
        <div class="container">
            <div class="row">
                <p>THIS USER ACCOUNT IS PRIVATE PLEASE FOLLOW IT FIRST</p>
            </div>
        </div>
    </section>
@endcannot
<!-- ==============================================
Modal Section
=============================================== -->
@foreach($user->posts as $post)
    @include("Post",["post"=>$post])
@endforeach

@include("Layouts.Footer")
