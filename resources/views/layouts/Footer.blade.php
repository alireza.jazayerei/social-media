<!-- ==============================================
	 Scripts
	 =============================================== -->
<script src="{{asset("assets\js\jquery.min.js")}}"></script>
<script src="{{asset("assets\js\bootstrap.min.js")}}"></script>
<script src="{{asset("assets\js\base.js")}}"></script>
<script src="{{asset("assets\plugins\slimscroll\jquery.slimscroll.js")}}"></script>
<script src="{{asset("assets\plugins\slimscroll\jquery.slimscroll.js")}}"></script>
<script src="{{asset("assets\js\imagePreview.js")}}"></script>
<script>
    $('#Slim,#Slim2').slimScroll({
        height:"auto",
        position: 'right',
        railVisible: true,
        alwaysVisible: true,
        size:"8px",
    });
</script>

<script type="text/javascript">
    $('#search').on('keyup',function(){
        $value=$(this).val();
        $.ajax({
            type : 'get',
            url : 'search',
            data:{'search':$value},
            success:function(data){
                $('#Slim').html(data);
            }
        });
    })
</script>
<script type="text/javascript">
    $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>

</body>
</html>
