<!DOCTYPE html>
<html lang="en">
<head>
    <!-- ==============================================
    Title and Meta Tags
    =============================================== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fluffs - Ultimate Bootstrap Social Network UI Kit</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta property="og:title" content="">
    <meta property="og:url" content="">
    <meta property="og:description" content="">
    <meta name="_token" content="{{csrf_token()}}">
    <!-- ==============================================
    Favicons
    =============================================== -->
    <link rel="icon" href="{{asset("assets/img/logo.jpg")}}">
    <link rel="apple-touch-icon" href="{{asset("img/favicons/apple-touch-icon.png")}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset("img/favicons/apple-touch-icon-72x72.png")}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset("img/favicons/apple-touch-icon-114x114.png")}}">

    <!-- ==============================================
    CSS
    =============================================== -->
    <link type="text/css" href="{{asset("assets\css\demos\photo.css")}}" rel="stylesheet">
    <link type="text/css" href="{{asset("assets\css\demos\interest.css")}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset("instagram.css-master/dist/instagram.css")}}">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- ==============================================
Navigation Section
=============================================== -->
<header class="tr-header">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"><i class="fab fa-instagram"></i> Fluffs</a>
            </div><!-- /.navbar-header -->
            <div class="navbar-left">
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                    </ul>
                </div>
            </div><!-- /.navbar-left -->


            <div class="navbar-right">
                <ul class="nav navbar-nav">

                    <li>
                        <div class="search-dashboard">


                            <form class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="false" aria-expanded="false">
                                <input placeholder="Search here" type="text" name="search" id="search">
                            </form>
                            <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                                <div class="dropdown-item noti-title">
                                </div>

                                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 416.983px;">
                                    <div class="slimscroll" style="max-height: 230px; overflow: hidden; width: auto; height: 416.983px;">
                                        <div id="Slim">



                                        </div><!--/ .Slim-->
                                        <div class="slimScrollBar" style="background: rgb(158, 165, 171) none repeat scroll 0% 0%; width: 8px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
                                        <div class="slimScrollRail" style="width: 8px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                    </div><!--/ .slimscroll-->
                                </div><!--/ .slimScrollDiv-->
                            </div><!--/ dropdown-menu-->

                        </div>
                    </li>

                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle" href="{{route("notifications.index")}}" role="button"
                           aria-haspopup="false" aria-expanded="false">
                            <i class="fa fa-bell noti-icon"></i>
                            <span
                                class="badge badge-danger badge-pill noti-icon-badge">{{auth()->user()->unreadNotifications()->where("type","!=","App\Notifications\UserRequestedNotification")->count()}}</span>
                        </a>

                    </li>

                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle" href="{{route("requests.index")}}"
                           aria-haspopup="false" aria-expanded="false">
                            <i class="fas fa-user-plus noti-icon"></i>
                            <span
                                class="badge badge-danger badge-pill noti-icon-badge">{{auth()->user()->notifications->where("type","App\Notifications\UserRequestedNotification")->where("read_at",NULL)->count()}}</span>
                        </a>

                    </li>

                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#"
                           role="button" aria-haspopup="false" aria-expanded="false">
                            <i class="fa fa-envelope noti-icon"></i>
                            <span class="badge badge-success badge-pill noti-icon-badge">6</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-lg dropdown-new">
                            <div class="dropdown-item noti-title">
                                <h6 class="m-0">
			   <span class="float-right">
			    <a href="" class="text-dark"><small>Clear All</small></a>
			   </span>Chat
                                </h6>
                            </div>

                            <div class="slimScrollDiv"
                                 style="position: relative; overflow: hidden; width: auto; height: 416.983px;">
                                <div class="slimscroll"
                                     style="max-height: 230px; overflow: hidden; width: auto; height: 416.983px;">
                                    <div id="Slim2">
                                        <a href="javascript:void(0);" class="dropdown-item notify-item nav-user">
                                            <div class="notify-icon"><img src="assets\img\users\1.jpg"
                                                                          class="img-responsive img-circle" alt="">
                                            </div>
                                            <p class="notify-details">Cristina Pride</p>
                                            <p class="text-muted font-13 mb-0 user-msg">Hi, How are you? What about our
                                                next meeting</p>
                                        </a><!--/ dropdown-item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item nav-user">
                                            <div class="notify-icon"><img src="assets\img\users\2.jpg"
                                                                          class="img-responsive img-circle" alt="">
                                            </div>
                                            <p class="notify-details">Sam Garret</p>
                                            <p class="text-muted font-13 mb-0 user-msg">Yeah everything is fine</p>
                                        </a><!--/ dropdown-item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item nav-user">
                                            <div class="notify-icon"><img src="assets\img\users\3.jpg"
                                                                          class="img-responsive img-circle" alt="">
                                            </div>
                                            <p class="notify-details">Karen Robinson</p>
                                            <p class="text-muted font-13 mb-0 user-msg">Wow that's great</p>
                                        </a><!--/ dropdown-item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item nav-user">
                                            <div class="notify-icon"><img src="assets\img\users\4.jpg"
                                                                          class="img-responsive img-circle" alt="">
                                            </div>
                                            <p class="notify-details">Sherry Marshall</p>
                                            <p class="text-muted font-13 mb-0 user-msg">Hi, How are you? What about our
                                                next meeting</p>
                                        </a><!--/ dropdown-item-->
                                        <a href="javascript:void(0);" class="dropdown-item notify-item nav-user">
                                            <div class="notify-icon"><img src="assets\img\users\5.jpg"
                                                                          class="img-responsive img-circle" alt="">
                                            </div>
                                            <p class="notify-details">Shawn Millard</p>
                                            <p class="text-muted font-13 mb-0 user-msg">Yeah everything is fine</p>
                                        </a><!--/ dropdown-item-->
                                    </div><!--/ .Slim-->
                                    <div class="slimScrollBar"
                                         style="background: rgb(158, 165, 171) none repeat scroll 0% 0%; width: 8px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
                                    <div class="slimScrollRail"
                                         style="width: 8px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
                                </div><!--/ slimscroll-->
                            </div> <!--/ slimScrollDiv-->
                            <a href="photo_chat.html" class="dropdown-item text-center notify-all">
                                View all <i class="fa fa-arrow-right"></i>
                            </a>
                        </div><!--/ dropdown-menu-->
                    </li>

                    <li class="dropdown mega-avatar">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                            <span class="avatar w-32"><img src="{{asset("storage/users/".auth()->user()->media->src)}}"
                                                           class="img-resonsive img-circle"
                                                           width="25" height="25" alt="..."></span>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">
                                {{auth()->user()->username}}
		                    </span>
                        </a>
                        <div class="dropdown-menu w dropdown-menu-scale pull-right">
                            <a class="dropdown-item" href="#"><span>New Story</span></a>
                            <a class="dropdown-item" href="#"><span>Become a Member</span></a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#"><span>Profile</span></a>
                            <a class="dropdown-item" href="#"><span>Settings</span></a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Sign out
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li><!-- /navbar-item -->

                </ul><!-- /.sign-in -->
            </div><!-- /.nav-right -->
        </div><!-- /.container -->
    </nav><!-- /.navbar -->
</header><!-- Page Header -->

<!-- ==============================================
Navbar Second Section
=============================================== -->
<section class="nav-sec">
    <div class="d-flex justify-content-between">
        <div class="p-2 nav-icon-lg {{$section==="home" ? "mint-green":'dark-black'}}">
            <a class="nav-icon" href="{{route("home")}}"><em class="fa fa-home"></em>
                <span>Home</span>
            </a>
        </div>
        <div class="p-2 nav-icon-lg {{$section==="explore" ? "mint-green":'clean-black'}}">
            <a class="nav-icon" href="photo_explore.html"><em class="fa fa-crosshairs"></em>
                <span>Explore</span>
            </a>
        </div>
        <div class="p-2 nav-icon-lg {{$section==="upload" ? "mint-green":'dark-black'}}">
            <a class="nav-icon" href="{{route("post.create")}}"><em class="fab fa-instagram"></em>
                <span>Upload</span>
            </a>
        </div>
        <div class="p-2 nav-icon-lg {{$section==="story" ? "mint-green":'clean-black'}}">
            <a class="nav-icon" href="photo_stories.html"><em class="fa fa-align-left"></em>
                <span>Stories</span>
            </a>
        </div>
        <div class="p-2 nav-icon-lg {{$section==="profile" ? "mint-green":'dark-black'}}">
            <a class="nav-icon" href="{{route("profile.index")}}"><em class="fa fa-user"></em>
                <span>Profile</span>
            </a>
        </div>
        <div class="p-2 nav-icon-lg {{$section==="saved" ? "mint-green":'clean-black'}}">

            <a class="nav-icon" href="photo_stories.html"><em class="far fa-bookmark"></em>
                <span>Saved Posts</span>
            </a>
        </div>

        <div class="p-2 nav-icon-lg {{$section==="direct" ? "mint-green":'dark-black'}}">
            <a class="nav-icon" href="photo_stories.html"><em class="fas fa-location-arrow"></em>
                <span>Directs</span>
            </a>
        </div>
    </div>
</section>
