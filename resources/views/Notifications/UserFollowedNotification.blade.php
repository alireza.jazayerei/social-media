<li>
    <div class="media first_child">
        <img src="{{asset("storage/users/".$notification->data["media"])}}" alt="" class="img-responsive img-circle">
        <div class="media_body">
            <p>
                <a href="{{route("profile.show",["user"=>$notification->data["username"]])}}"><b>{{$notification->data["username"]}}</b></a>
                followed you
            </p>
            <h6>{{$notification->created_at->diffForHumans()}}</h6>
            <div class="btn_group">

                <form action="{{route("follow.store",["user"=>$notification->data["username"]])}}" method="post">
                    @csrf
                    @if($followingStatus=auth()->user()->isFollowing($user=App\User::find($notification->data["id"])))
                        @method("DELETE")
                    @endif
                    <button
                        class="kafe-btn kafe-btn-mint">
                        @if($followingStatus)
                            Unfollow
                        @elseif(auth()->user()->hasRequested($user))
                            Requested
                        @else
                            Follow
                        @endif
                    </button>
                </form>
            </div>
        </div>
    </div>
</li>
