<li>
    <div class="media first_child">
        <img src="{{asset("storage/users/".$notification->data["userMedia"])}}" alt=""
             class="img-responsive img-circle">
        <div class="media_body">
            <p>
                <a href="{{route("profile.show",["user"=>$notification->data["username"]])}}"><b>{{$notification->data["username"]}}</b></a>
                commented "{{$notification->data["commentBody"]}}" on your <a href="{{route("post.show",["post"=>$notification->data["post"]])}}">POST</a>
            </p>
            <h6>{{$notification->created_at->diffForHumans()}}</h6>
        </div>
    </div>
</li>
