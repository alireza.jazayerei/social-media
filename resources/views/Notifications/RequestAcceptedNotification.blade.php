<li>
    <div class="media first_child">
        <img src="{{asset("storage/users/".$notification->data["media"])}}" alt=""
             class="img-responsive img-circle">
        <div class="media_body">
            <p>
                <a href="{{route("profile.show",["user"=>$notification->data["username"]])}}"><b>{{$notification->data["username"]}}</b></a>
                accepted your follow request
            </p>
            <h6>{{$notification->created_at->diffForHumans()}}</h6>
        </div>
    </div>
</li>
