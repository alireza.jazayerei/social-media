<li>
    <div class="media first_child">
        <img src="{{asset("storage/users/".$notification->data["media"])}}" alt="" class="img-responsive img-circle">
        <div class="media_body">
            <p>
                <a href="{{route("profile.show",["user"=>$notification->data["username"]])}}"><b>{{$notification->data["username"]}}</b></a>
                requested to follow you
            </p>
            <h6>{{$notification->created_at->diffForHumans()}}</h6>
            <div class="btn_group">

                <form action="{{route("requests.store",["user"=>$notification->data["username"]])}}" method="post">
                    @csrf
                    <button class="kafe-btn kafe-btn-mint">Accept</button>
                </form>
                <form action="{{route("requests.destroy",["user"=>$notification->data["username"]])}}" method="post">
                    @csrf
                    @method("DELETE")
                    <button class="kafe-btn kafe-btn-danger">Deny</button>
                </form>
            </div>
        </div>
    </div>
</li>
