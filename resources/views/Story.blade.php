@include("Layouts.Header",["section"=>"story"])

<section class="stories">
    <div class="container-fluid">
        <div class="row">

            <div class="stories-base">
                <ul class="list-group">
                    <li class="list-group-item full-width">
                        <div class="pull-left">
                            <button class="btn btn-stories">
                                <i class="fa fa-plus"></i>
                            </button>
                            <a href="#myModal" data-toggle="modal">
                                <img src="assets\img\users\1.jpg" alt="" class="img-circle">
                            </a>

                            <a href=""><img src="assets\img\users\2.jpg" alt="" class="img-circle"></a>
                            <a href=""><img src="assets\img\users\3.jpg" alt="" class="img-circle"></a>
                        </div><!--/ pull-left -->
                    </li>
                </ul>
            </div><!--/ followers-base -->

        </div>
    </div>
</section>
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <img class="img-responsive" src="assets\img\posts\1.jpg" alt="Image">
            </div><!--/ modal-body -->
        </div><!--/ modal-content -->
    </div><!--/ modal-dialog -->
</div><!--/ modal -->

@include("Layouts.Footer")
