@include("Layouts.Header",["section"=>""])


<section class="notifications">
    <div class="container">
        <div class="row">

            <div class="col-lg-8 col-lg-offset-2">
                <ul>
                    @foreach($notifications as $notification)
                        @include("Notifications.".class_basename($notification->type),["notification"=>$notification])
                    @endforeach

                </ul>

            </div>

        </div><!--/ row-->
    </div><!--/ container -->
</section>


@include("Layouts.Footer")
