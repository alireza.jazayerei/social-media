@include("Layouts.Header",["section"=>"profile"])

<section class="user-profile">

    <br><br><br>
    <br><br>
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">
                <div class="post-content">
                    <div class="author-post text-center">
                        <form action="{{route("profile.update")}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method("PATCH")
                            <div class="image-upload">
                                <label for="file-input">
                                    <img class="img-fluid img-circle"
                                         src="{{asset("storage/users/".$user->media->src)}}" alt="Image">
                                </label>
                                <input id="file-input" type="file" name="image" style="display: none"
                                       onchange="form.submit()"/>
                            </div>

                    </div><!-- /author -->
                </div><!-- /.post-content -->
            </div><!-- /col-sm-12 -->

        </div><!--/ row-->
    </div><!--/ container -->
</section><!--/ profile -->

<!-- ==============================================
User Profile Section
=============================================== -->
<section class="details">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <div class="details-box row">
                    <div class="col-lg-9">
                        <div class="content-box">
                            <h4><input type="text" name="name" value="{{$user->name}}"
                                       placeholder="your Name" style="border: none" onchange="form.submit()"></h4>
                            <p><textarea name="bio" style="border: none;width: 100%" placeholder="your bio"
                                         onchange="form.submit()">{{$user->bio}}</textarea></p>
                            <small><span><input type="text" name="website" value="{{$user->website}}"
                                                placeholder="your website address" style="border: none"
                                                onchange="form.submit()"></span></small>

                        </div><!--/ media -->
                    </div>
                    <div class="col-lg-3">
                        <div class="follow-box mb-3">
                            <a href="" class="kafe-btn kafe-btn-mint">Change Email</a>
                            <br><br>
                            <a href="{{route("password.change.show")}}" class="kafe-btn kafe-btn-danger">New
                                Password</a>
                            <br><br>
                            <input type="checkbox" name="is_private"
                                   onchange="form.submit()" {{$user->is_private?'checked':''}}>
                            <label>Private</label>
                            </form>
                        </div>
                    </div><!--/ dropdown -->
                </div>
            </div><!--/ details-box -->

        </div>
    </div>
    </div><!--/ container -->
</section><!--/ profile -->

<!-- ==============================================
Home Menu Section
=============================================== -->
<section class="home-menu">
    <div class="container">
        <div class="row">
            <div class="menu-category">
                <ul class="menu">
                    <li class="current-menu-item">Posts <span>{{$user->posts_count}}</span></li>
                    <li><a href="{{route("follower.index")}}">Followers <span>{{$user->followers_count}}</span></a></li>
                    <li><a href="{{route("following.index")}}">Following <span>{{$user->followings_count}}</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- ==============================================
News Feed Section
=============================================== -->
<section class="newsfeed">
    <div class="container">
        <div class="row">
            @foreach($user->posts as $post)
                <div class="col-lg-4">

                    <div class="filter-{{$post->filter}}">

                        <div class="explorebox"
                             style="background: linear-gradient( rgba(34,34,34,0.2), rgba(34,34,34,0.2)), url({{asset("storage/users/".$post->media->src)}}) no-repeat;
                                 background-size: cover;
                                 background-position: center center;
                                 -webkit-background-size: cover;
                                 -moz-background-size: cover;
                                 -o-background-size: cover;">

                            <div class="explore-top">
                                <div class="explore-like">
                                    <i class="fa fa-heart"></i>
                                    <span>{{$post->likes_count}}</span></div>
                                <a href="{{route("post.edit",["post"=>$post])}}">
                                    <div class="explore-circle pull-right"><i class="fas fa-pen-square"></i></div>
                                    <div class="explore-circle pull-right"><a
                                            href="{{ route('post.destroy',["post"=>$post]) }}"
                                            onclick="event.preventDefault();
                                                document.getElementById('delete-{{$post->id}}').submit();"><i
                                                class="fas fa-trash-alt"></i> </a></div>
                                    <div class="explore-circle pull-right"><a href="#{{$post->id}}" data-toggle="modal"><i
                                                class="fas fa-eye"></i></a></div>

                                </a>
                            </div>


                            <form id="delete-{{$post->id}}" action="{{ route('post.destroy',["post"=>$post]) }}" method="POST" style="display: none;">
                                @csrf
                                @method("DELETE")
                            </form>


                        </div>

                    </div>

                </div>
            @endforeach
        </div>


    </div><!--/ container -->
</section><!--/ newsfeed -->

<!-- ==============================================
Modal Section
=============================================== -->
@foreach($user->posts as $post)
    @include("Post",["post"=>$post,"likedPosts"=>$likedPosts])
@endforeach

@include("Layouts.Footer")
