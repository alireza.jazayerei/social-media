<!DOCTYPE html>
<html lang="en">
<head>

    <!-- ==============================================
    Title and Meta Tags
    =============================================== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fluffs - Ultimate Bootstrap Social Network UI Kit</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta property="og:title" content="">
    <meta property="og:url" content="">
    <meta property="og:description" content="">

    <!-- ==============================================
    Favicons
    =============================================== -->
    <link rel="icon" href="assets/img/logo.jpg">
    <link rel="apple-touch-icon" href="img/favicons/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicons/apple-touch-icon-114x114.png">

    <!-- ==============================================
    CSS
    =============================================== -->
    <link type="text/css" href="{{asset("assets/css/demos/photo.css")}}" rel="stylesheet">

    <!-- ==============================================
    Feauture Detection
    =============================================== -->


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- ==============================================
Header Section
=============================================== -->
<section class="login">
    <div class="container">
        <div class="banner-content">

            <h1><i class="fa fa-smile"></i> Fluffs</h1>
            <form method="POST" action="{{ route('login') }}" class="form-signin">
                @csrf

                <h3 class="form-signin-heading">Please sign in</h3>
                @include("Error",["field"=>"username"])
                @include("Error",["field"=>"email"])
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Username or Email" name="login"
                           value="{{ old('username') ?: old('email') }}">
                </div>
                @include("Error",["field"=>"password"])
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
                <div class="form-group">
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>Remember me
                </div>

                <button class="kafe-btn kafe-btn-mint btn-block" type="submit" name="subm">Sign in</button>
                <br>
                <a class="btn btn-dark " href="/register" role="button">Don't have an account yet? Register
                    Here.</a>
                <a class="btn btn-danger " href="/login/google" role="button">Login with GOOGLE</a>
                <a class="btn btn-dark " href="{{route("password.request")}}" role="button">Forgot your password?</a>
            </form>

        </div><!--/. banner-content -->
    </div><!-- /.container -->
</section>


<!-- ==============================================
Scripts
=============================================== -->
<script src="assets\js\jquery.min.js"></script>
<script src="assets\js\bootstrap.min.js"></script>
<script src="assets\js\base.js"></script>

</body>
</html>
