@include("Layouts.Header",["section"=>"upload"])

<section class="upload">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">


                <div class="box">
                    <form action="{{route("post.store")}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @include("Error",["field"=>"image"])
                        @include("Error",["field"=>"caption"])
                        <textarea name="caption" class="form-control no-border" rows="3" placeholder="Type something..."></textarea>

                        <div class="box-footer clearfix">
                            <button type="submit" class="kafe-btn kafe-btn-mint-small pull-right btn-sm">Post</button>
                            <div class="input-group">
                                <ul class="nav nav-pills nav-sm">
                                    <span class="btn btn-default btn-file">

                                        <label for="imgInp">
                                             <li class="nav-item"><i class="fa fa-camera text-muted"></i></li>
                                        </label>
                                        <input id="imgInp" type="file" name="image" style="display: none"/>
                                    </span>
                                </ul>
                            </div>
                        </div>
                </div>

            </div>
        </div>

        <div class="row one-row">
            <div class="col-lg-12">
                <h4>choose a filter</h4>
            </div>
        </div>

        <div class="row">
            @include("FilterPreview",['filter'=>NULL])
            @include("FilterPreview",['filter'=>"1977"])
            @include("FilterPreview",['filter'=>"willow"])
        </div><!--/ row-->

        <div class="row">
            @include("FilterPreview",['filter'=>"sutro"])
            @include("FilterPreview",['filter'=>"reyes"])
            @include("FilterPreview",['filter'=>"poprocket"])
        </div><!--/ row-->

        <div class="row">
            @include("FilterPreview",['filter'=>"inkwell"])
            @include("FilterPreview",['filter'=>"nashville"])
            @include("FilterPreview",['filter'=>"moon"])
        </div><!--/ row-->
        </form>

    </div><!--/ container -->
</section><!--/ newsfeed -->

@include("Layouts.Footer")
