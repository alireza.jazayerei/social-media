@include("Layouts.Header",["section"=>"home"])
<section class="newsfeed">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <div class="img-story-list">
                    <a href="#">
                        <div class="storybox" style="background: linear-gradient( rgba(34,34,34,0.78), rgba(34,34,34,0.78)), url('assets/img/posts/2.gif') no-repeat;
		          background-size: cover;
                  background-position: center center;
                  -webkit-background-size: cover;
                  -moz-background-size: cover;
                  -o-background-size: cover;">
                            <div class="story-body text-center">
                                <div class=""><img class="img-circle" src="assets\img\users\10.jpg" alt="user"></div>
                                <h4>Clifford Graham</h4>
                                <p>2 hours ago</p>
                            </div>
                        </div>
                    </a>

                    <a href="#my_modal">
                        <div class="storybox" style="background: linear-gradient( rgba(34,34,34,0.78), rgba(34,34,34,0.78)), url('assets/img/posts/3.gif') no-repeat;
		          background-size: cover;
                  background-position: center center;
                  -webkit-background-size: cover;
                  -moz-background-size: cover;
                  -o-background-size: cover;">
                            <div class="story-body text-center">
                                <div class=""><img class="img-circle" src="assets\img\users\13.jpeg" alt="user"></div>
                                <h4>Eleanor Harper</h4>
                                <p>4 hours ago</p>
                            </div>
                        </div>
                    </a>

                    <a href="#">
                        <div class="storybox" style="background: linear-gradient( rgba(34,34,34,0.78), rgba(34,34,34,0.78)), url('assets/img/posts/4.jpg') no-repeat;
		          background-size: cover;
                  background-position: center center;
                  -webkit-background-size: cover;
                  -moz-background-size: cover;
                  -o-background-size: cover;">
                            <div class="story-body text-center">
                                <div class=""><img class="img-circle" src="assets\img\users\12.jpg" alt="user"></div>
                                <h4>Sean Coleman</h4>
                                <p>5 hours ago</p>
                            </div>
                        </div>
                    </a>

                    <a href="#myModals" data-toggle="modal">
                        <div class="storybox" style="background: linear-gradient( rgba(34,34,34,0.78), rgba(34,34,34,0.78)), url('assets/img/posts/15.jpg') no-repeat;
		          background-size: cover;
                  background-position: center center;
                  -webkit-background-size: cover;
                  -moz-background-size: cover;
                  -o-background-size: cover;">
                            <div class="story-body text-center">
                                <div class=""><img class="img-circle" src="assets\img\users\15.jpg" alt="user"></div>
                                <h4>Vanessa Wells</h4>
                                <p>5 hours ago</p>
                            </div>
                        </div>
                    </a>

                    <div class="trending-box hidden-xs hidden-md">
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="photo_stories.html"><h4>More stories</h4></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                @foreach($posts as $post)
                    <div class="cardbox">
                        <div class="cardbox-heading">
                            <!-- START dropdown-->
                            <div class="dropdown pull-right">
                                <button class="btn btn-secondary btn-flat btn-flat-icon" type="button"
                                        data-toggle="dropdown" aria-expanded="false">
                                    <em class="fa fa-ellipsis-h"></em>
                                </button>
                                <div class="dropdown-menu dropdown-scale dropdown-menu-right" role="menu"
                                     style="position: absolute; transform: translate3d(-136px, 28px, 0px); top: 0px; left: 0px; will-change: transform;">
                                    {{--//todo --}}
                                    <a class="dropdown-item" href="#">save post</a>
                                    <a class="dropdown-item" href="#">Stop following</a>
                                </div>
                            </div><!--/ dropdown -->
                            <!-- END dropdown-->
                            <div class="media m-0">
                                <div class="d-flex mr-3">
                                    <a href="{{route("profile.show",["user"=>$post->user->username])}}"><img
                                            class="img-responsive img-circle"
                                            src="{{asset("storage/users/".$post->user->media->src)}}" alt="User"></a>
                                </div>
                                <div class="media-body">
                                    <p class="m-0">{{$post->user->username}}</p>
                                    <small><span>{{$post->created_at->diffForHumans()}}</span></small>
                                </div>
                            </div><!--/ media -->
                        </div><!--/ cardbox-heading -->

                        <div class="cardbox-item">
                            <a href="#{{$post->id}}" data-toggle="modal">
                                <div class="filter-{{$post->filter}}">
                                    <img class="img-responsive" width="100%" height="100%"
                                         src="{{asset("storage/users/".$post->media->src)}}" alt="MaterialImg">
                                </div>
                            </a>
                        </div><!--/ cardbox-item -->
                        <div class="cardbox-base">
                            <p style="margin:10px 15px 10px 20px">
                                {{$post->caption}}
                            </p>
                        </div><!--/ cardbox-base -->
                        <div class="cardbox-like">
                            <ul>
                                <li>
                                    <a href="{{route("like.store",["post"=>$post])}}"
                                       onclick="event.preventDefault();
                                           document.getElementById('{{"like".$post->id}}').submit();">
                                        <i class="{{$likedPosts->contains($post->id)?'fa fa-heart':'far fa-heart'}}"></i></a>
                                    <span>{{$post->likes_count}}</span>
                                </li>

                                <li><a title="" class="com"><i class="fa fa-comments"></i></a> <span
                                        class="span-last">{{$post->comments_count}}</span></li>
                            </ul>
                        </div><!--/ cardbox-like -->

                    </div>
                @endforeach
            </div><!--/ col-lg-6 -->
            <div class="col-lg-3">

                <div class="suggestion-box full-width">
                    <div class="suggestions-list">
                        <div class="suggestion-body">
                            <img class="img-responsive img-circle" src="assets\img\users\17.jpeg" alt="Image">
                            <div class="name-box">
                                <h4>Anthony McCartney</h4>
                                <span>@antony</span>
                            </div>
                            <span><i class="fa fa-plus"></i></span>
                        </div>
                        <div class="suggestion-body">
                            <img class="img-responsive img-circle" src="assets\img\users\16.jpg" alt="Image">
                            <div class="name-box">
                                <h4>Sean Coleman</h4>
                                <span>@sean</span>
                            </div>
                            <span><i class="fa fa-plus"></i></span>
                        </div>
                        <div class="suggestion-body">
                            <img class="img-responsive img-circle" src="assets\img\users\14.jpg" alt="Image">
                            <div class="name-box">
                                <h4>Francis Long</h4>
                                <span>@francis</span>
                            </div>
                            <span><i class="fa fa-plus"></i></span>
                        </div>
                        <div class="suggestion-body">
                            <img class="img-responsive img-circle" src="assets\img\users\11.jpg" alt="Image">
                            <div class="name-box">
                                <h4>Vanessa Wells</h4>
                                <span>@vannessa</span>
                            </div>
                            <span><i class="fa fa-plus"></i></span>
                        </div>
                        <div class="suggestion-body">
                            <img class="img-responsive img-circle" src="assets\img\users\9.jpg" alt="Image">
                            <div class="name-box">
                                <h4>Anna Morgan</h4>
                                <span>@anna</span>
                            </div>
                            <span><i class="fa fa-plus"></i></span>
                        </div>
                        <div class="suggestion-body">
                            <img class="img-responsive img-circle" src="assets\img\users\8.jpg" alt="Image">
                            <div class="name-box">
                                <h4>Clifford Graham</h4>
                                <span>@clifford</span>
                            </div>
                            <span><i class="fa fa-plus"></i></span>
                        </div>
                    </div><!--suggestions-list end-->
                </div>

            </div>

        </div><!--/ row -->
    </div><!--/ container -->
</section><!--/ newsfeed -->

<!-- ==============================================
Modal Section
=============================================== -->
@foreach($posts as $post)
    @include("Post",["post"=>$post,"likedPosts"=>$likedPosts])
@endforeach
<!-- ==============================================
Modal Section
=============================================== -->
@include("Layouts.Footer")
