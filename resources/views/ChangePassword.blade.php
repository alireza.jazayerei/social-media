@include("Layouts.Header",["section"=>"upload"])


<section class="newsfeed">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
            </div>
            <div class="col-lg-4">
                <form method="POST" action="{{ route('password.change.store') }}">
                    @csrf
                    <div class="form-group row">
                        @if(session()->has('passwordError'))
                            {{ session()->get('passwordError') }}
                        @endif
                        <input  type="password" placeholder="your password"
                               class="form-control"
                               name="oldPassword" required>
                        <br><br>
                        <input id="password" type="password" placeholder="new password"
                               class="form-control"
                               name="newPassword" required>
                        <br>
                        <input  type="password" placeholder="re enter new password"
                               class="form-control"
                               name="newPassword_confirmation" required>

                        @error('newPassword')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group row mb-0">
                        <button type="submit" class="btn btn-primary">
                            submit
                        </button>
                    </div>


                </form>
            </div>

        </div>


    </div><!--/ container -->
</section><!--/ newsfeed -->


@include("Layouts.Footer")
