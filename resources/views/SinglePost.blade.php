@include("Layouts.Header",["section"=>""])

<section class="single-post">
    <div class="container">
        <div class="row">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">

                        <div class="row">

                            <div class="col-md-8 modal-image">
                                <img class="img-responsive" src="{{asset("storage/users/".$post->media->src)}}" alt="Image">
                            </div><!--/ col-md-8 -->
                            <div class="col-md-4 modal-meta">
                                <div class="modal-meta-top">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                                    </button><!--/ button -->
                                    <div class="img-poster clearfix">
                                       <img class="img-responsive img-circle" src="{{asset("storage/users/".$post->user->media->src)}}" alt="Image">
                                        <strong><a href="{{route("profile.show",["user"=>$post->user->username])}}">{{$post->user->username}}</a></strong>
                                        <span>{{$post->created_at->diffForHumans()}}</span><br>
                                    </div><!--/ img-poster -->

                                    <ul class="img-comment-list">
                                        @foreach($post->comments as $comment)
                                            <li>
                                                <div class="comment-img">
                                                    <img src="{{asset("storage/users/".$comment->user->media->src)}}"
                                                         class="img-responsive img-circle"
                                                         alt="Image">
                                                </div>
                                                <div class="comment-text">
                                                    <strong><a href="{{route("profile.show",["user"=>$comment->user->username])}}">{{$comment->user->username}}</a></strong>
                                                    <p>{{$comment->body}}</p>

                                                    <form id="{{"likeComment".$comment->id}}"
                                                          action="{{route("comment.like.store",["comment"=>$comment])}}"
                                                          method="post">
                                                        @if($likedComments->contains($comment->id))
                                                            @method("DELETE")
                                                        @endif
                                                        @csrf
                                                    </form>
                                                    <a class="modal-like"
                                                       href="{{route("comment.like.store",["comment"=>$comment])}}"
                                                       onclick="event.preventDefault();
                                                           document.getElementById('{{"likeComment".$comment->id}}').submit();">
                                                        <i class="{{$likedComments->contains($comment->id)?'fa fa-heart':'far fa-heart'}}"></i></a>
                                                    <span class="modal-one">{{$comment->likes_count}}</span> |

                                                    <span class="date sub-text">{{$comment->created_at->diffForHumans()}}</span>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul><!--/ comment-list -->

                                    <div class="modal-meta-bottom">
                                        <ul>
                                            <li>
                                                <form id="{{"like".$post->id}}" action="{{route("like.store",["post"=>$post])}}"
                                                      method="post">
                                                    @csrf
                                                    @if($hasLikedPost)
                                                        @method("DELETE")
                                                    @endif
                                                </form>
                                                <a class="modal-like" href="{{route("like.store",["post"=>$post])}}"
                                                   onclick="event.preventDefault();
                                                       document.getElementById('{{"like".$post->id}}').submit();">
                                                    <i class="{{$hasLikedPost?'fa fa-heart':'far fa-heart'}}"></i></a>
                                                <span class="modal-one">{{$post->likes_count}}</span> |
                                                <a class="modal-comment"><i
                                                        class="fa fa-comments"></i></a><span>{{$post->comments_count}}</span>
                                            </li>
                                            <li>
                                        <span class="thumb-xs">
                                        <img class="img-responsive img-circle"
                                             src="{{asset("storage/users/".auth()->user()->media->src)}}"
                                             alt="Image">
                                       </span>
                                                <div class="comment-body">
                                                    <form action="{{route("comment.store",["post"=>$post])}}" method="post">
                                                        @csrf
                                                        <input class="form-control input-sm" name="body" type="text"
                                                               placeholder="Write your comment...">
                                                    </form>
                                                </div><!--/ comment-body -->
                                            </li>
                                        </ul>
                                    </div><!--/ modal-meta-bottom -->

                                </div><!--/ modal-meta-top -->
                            </div><!--/ col-md-4 -->

                        </div><!--/ row -->
                    </div><!--/ modal-body -->

                </div><!--/ modal-content -->
            </div><!--/ modal-dialog -->

        </div><!--/ modal-dialog -->
    </div><!--/ modal-dialog -->
</section>

@include("Layouts.Footer")
