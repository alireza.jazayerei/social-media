@include("Layouts.Header",["section"=>"profile"])

<section class="followers">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-8 col-lg-offset-2">

                <div class="followers-box full-width">
                    <div class="followers-list">
                        @foreach($followers as $follower)
                            <div class="followers-body">
                                <img class="img-responsive img-circle"
                                     src="{{asset("storage/users/".$follower->media->src)}}" alt="">
                                <div class="name-box">
                                    <h4>{{$follower->name}}</h4>
                                    <a href="{{route("profile.show",["user"=>$follower])}}">
                                        <span>{{"@".$follower->username}}</span>
                                    </a>

                                </div><!--/ name-box -->

                                <form id="{{$follower->id}}" action="{{route("follow.store",["user"=>$follower])}}" method="post">
                                    @csrf
                                </form>

                                <span><a href="{{route("follow.store",["user"=>$follower])}}"
                                         class="kafe-btn kafe-btn-mint-small" onclick="event.preventDefault();
                                        document.getElementById('{{$follower->id}}').submit();">{{auth()->user()->isFollowing($follower)?"Unfollow":"Follow"}}</a></span>
                            </div><!--/ followers-body -->
                        @endforeach

                    </div><!--suggestions-list end-->
                </div>

            </div>

        </div><!--/ row-->
    </div><!--/ container -->
</section>

@include("Layouts.Footer")
