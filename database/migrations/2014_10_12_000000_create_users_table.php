<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDb';
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('api_token', 80)->unique()->nullable()->default(null);
            $table->boolean('is_private')->default(false);
            $table->string('bio', 240)->nullable();
            $table->string('website', 240)->nullable();
            $table->unsignedInteger('media_id')->nullable();
            $table->unsignedBigInteger("posts_count")->default("0");
            $table->unsignedBigInteger("followers_count")->default("0");
            $table->unsignedBigInteger("followings_count")->default("0");
            $table->rememberToken();
            $table->timestamps();
            $table->foreign("media_id")->references("id")->on("media")
                ->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
