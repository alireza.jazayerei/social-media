<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'follows';

    /**
     * Run the migrations.
     * @table follows
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('following_id');
            $table->timestamps();

            $table->foreign("user_id")->references("id")->on("users")
                ->onDelete("cascade")->onDelete("cascade");
            $table->foreign("following_id")->references("id")->on("users")
                ->onDelete("cascade")->onDelete("cascade");
            $this->unique(["user_id","following_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
