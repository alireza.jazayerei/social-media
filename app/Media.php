<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';
    protected $fillable = ["src", "hash"];

    public static function firstOrUpload($file)
    {

        if ($media = Media::isAvailable($file)) {
            return $media->id;
        }
        return Media::create([
            "hash" => md5_file($file),
            "src" => $file->store("", "user_profile"),
        ])->id;
    }

    public static function isAvailable($file)
    {
        return Media::where("hash", md5_file($file))->first();
    }

}
