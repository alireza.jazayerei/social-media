<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RequestAcceptedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $followedUser;

    /**
     * Create a new notification instance.
     *
     * @param $followedUser
     */
    public function __construct($followedUser)
    {
        $this->followedUser = $followedUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            "username" => $this->followedUser->username,
            "media" => $this->followedUser->media->src,
        ];
    }
}
