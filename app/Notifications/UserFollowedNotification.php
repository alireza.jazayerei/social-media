<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserFollowedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $followingUser;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($followingUser)
    {
        $this->followingUser = $followingUser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            "id" => $this->followingUser->id,
            "username" => $this->followingUser->username,
            "media" => $this->followingUser->media->src,
        ];
    }
}
