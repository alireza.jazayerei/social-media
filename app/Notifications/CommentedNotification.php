<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CommentedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $user;
    public $post;
    public $comment;

    /**
     * Create a new notification instance.
     *
     * @param $user
     * @param $post
     * @param $comment
     */
    public function __construct($user, $post,$comment)
    {
        $this->user = $user;
        $this->post = $post;
        $this->comment = $comment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            "username" => $this->user->username,
            "userMedia" => $this->user->media->src,
            "commentBody" => $this->comment->body,
            "post" => $this->post->id,
        ];
    }
}
