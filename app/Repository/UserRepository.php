<?php


namespace App\Repository;


trait UserRepository
{
    public function likedPosts()
    {
        return $this->likesPost()->pluck("id");
    }

    public function likedComments()
    {
        return $this->likesComment()->pluck("id");
    }

    public function homePosts()
    {
        return $this->posts()
            ->with(["user.media", "media", "comments.user.media"])
            ->orwhereIn("user_id", $this->followingId())->get();
    }

    public function followingId()
    {
        return $this->followings()->pluck("id");
    }

    public function hasLikedPost($posts)
    {
        return $this->likesPost()->whereIn("likable_id", $posts)->pluck("id");
    }

    public function hasLikedComment($comments)
    {
        return $this->likesComment()->whereIn("likable_id", $comments)->pluck("id");
    }

    public function hasRequested($user)
    {
        return $this->requesting()->where("requesting_id", $user->id)->exists();
    }
    
    public function isFollowing($user)
    {
        return $this->followings()->where("following_id", $user->id)->exists();
    }
}
