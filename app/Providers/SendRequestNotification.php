<?php

namespace App\Providers;

use App\Notifications\UserRequestedNotification;
use App\Providers\FollowRequested;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendRequestNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FollowRequested  $event
     * @return void
     */
    public function handle(FollowRequested $event)
    {
        $event->requestedUser->notify(new UserRequestedNotification(auth()->user()));
    }
}
