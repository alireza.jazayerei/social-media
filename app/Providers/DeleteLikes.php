<?php

namespace App\Providers;

use App\Comment;
use App\Post;
use App\Providers\PostDeleted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;

class DeleteLikes
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PostDeleted  $event
     * @return void
     */
    public function handle(PostDeleted $event)
    {
        DB::table("likables")->where("likable_id",$event->post->id)->where("likable_type",Post::class)->delete();
        DB::table("likables")->whereIn("likable_id",$event->post->comments->pluck("id"))->where("likable_type",Comment::class)->delete();
    }
}
