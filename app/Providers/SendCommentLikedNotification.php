<?php

namespace App\Providers;

use App\Notifications\CommentLikedNotification;
use App\Notifications\PostLikedNotification;
use App\Providers\CommentLiked;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendCommentLikedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param CommentLiked $event
     * @return void
     */
    public function handle(CommentLiked $event)
    {
        if (! $event->comment->user->is(auth()->user())) {
            $event->comment->user->notify(new CommentLikedNotification($event->user, $event->comment));
        }
    }
}
