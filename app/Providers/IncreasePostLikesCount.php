<?php

namespace App\Providers;

use App\Providers\PostLiked;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class IncreasePostLikesCount
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param PostLiked $event
     * @return void
     */
    public function handle(PostLiked $event)
    {
        $event->post->increment("likes_count");
    }
}
