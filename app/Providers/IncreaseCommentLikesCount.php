<?php

namespace App\Providers;

use App\Providers\CommentLiked;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class IncreaseCommentLikesCount
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommentLiked  $event
     * @return void
     */
    public function handle(CommentLiked $event)
    {
        $event->comment->increment("likes_count");
    }
}
