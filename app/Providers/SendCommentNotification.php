<?php

namespace App\Providers;

use App\Notifications\CommentedNotification;
use App\Providers\Commented;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendCommentNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Commented  $event
     * @return void
     */
    public function handle(Commented $event)
    {
        if (! $event->post->user->is(auth()->user())) {
            $event->post->user->notify(new CommentedNotification($event->user, $event->post, $event->comment));
        }
    }
}
