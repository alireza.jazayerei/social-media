<?php

namespace App\Providers;

use App\Notifications\PostLikedNotification;
use App\Providers\PostLiked;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendPostLikedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param PostLiked $event
     * @return void
     */
    public function handle(PostLiked $event)
    {
        if (!$event->post->user->is(auth()->user())) {
            $event->post->user->notify(new PostLikedNotification($event->user, $event->post));
        }
    }
}
