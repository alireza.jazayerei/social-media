<?php

namespace App\Providers;

use App\Providers\UserFollowed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class IncreaseFollowingCount
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param RequestAccepted $event
     * @return void
     */
    public function handle($event)
    {
        $event->followingUser->increment("followings_count");
    }
}
