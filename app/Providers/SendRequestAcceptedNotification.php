<?php

namespace App\Providers;

use App\Notifications\RequestAcceptedNotification;
use App\Providers\RequestAccepted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendRequestAcceptedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param RequestAccepted $event
     * @return void
     */
    public function handle(RequestAccepted $event)
    {
        $event->followingUser->notify(new RequestAcceptedNotification($event->followedUser));
    }
}
