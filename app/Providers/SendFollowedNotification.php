<?php

namespace App\Providers;

use App\Notifications\UserFollowedNotification;
use App\Providers\UserFollowed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendFollowedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserFollowed $event
     * @return void
     */
    public function handle(UserFollowed $event)
    {
        $event->followedUser->notify(new UserFollowedNotification($event->followingUser));
    }
}
