<?php

namespace App\Providers;

use App\Providers\UserFollowed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class IncreaseFollowerCount
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param RequestAccepted $event
     * @return void
     */
    public function handle($event)
    {
        $event->followedUser->increment("followers_count");
    }
}
