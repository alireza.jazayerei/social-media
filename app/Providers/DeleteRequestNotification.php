<?php

namespace App\Providers;

use App\Notifications\UserRequestedNotification;
use App\Providers\RequestAccepted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class DeleteRequestNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RequestAccepted  $event
     * @return void
     */
    public function handle(RequestAccepted $event)
    {
        $event->followedUser->notifications()->where("type",UserRequestedNotification::class)->where("read_at","!=",NULL)->delete();
    }
}
