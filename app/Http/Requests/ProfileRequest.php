<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "website" => ["nullable", "max:240", "url"],
            "name" => ["nullable", "alpha", 'max:191'],
            "bio" => ["nullable", "max:240"],
        ];
    }
}
