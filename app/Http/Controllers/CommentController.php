<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Post;
use App\Providers\Commented;

class CommentController extends Controller
{
    public function store(Post $post, CommentRequest $request)
    {
        $this->authorize("view", [$post, auth()->user()->isFollowing($post->user)]);
        $comment = auth()->user()->comments()->create([
            "post_id" => $post->id,
            "body" => $request->input("body"),
        ]);

        event(new Commented(auth()->user(), $post,$comment));

        return back();
    }
}
