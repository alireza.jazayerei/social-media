<?php

namespace App\Http\Controllers;

use App\Providers\FollowRequested;
use App\Providers\UserFollowed;
use App\User;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;


class FollowController extends Controller
{
    public function store(User $user)
    {
        if ($user->is_private) {

            auth()->user()->requesting()->toggle([
                "requesting_id" => $user->id,
            ]);

            event(new FollowRequested($user));

            return back();
        }

        auth()->user()->followings()->toggle([
            "following_id" => $user->id,
        ]);

        event(new UserFollowed(auth()->user(), $user));

        return back();
    }

    public function destroy(User $user)
    {
        User::query()->first();
        if (auth()->user()->hasRequested($user)) {
            auth()->user()->requesting()->toggle([
                "requesting_id" => $user->id,
            ]);
            return back();
        }

        auth()->user()->followings()->toggle([
            "following_id" => $user->id,
        ]);

        $user->decrement("followers_count");
        auth()->user()->decrement("followings_count");

        return back();
    }
}
