<?php

namespace App\Http\Controllers;

use App\Notifications\CommentedNotification;
use App\User;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index()
    {
        $notifications = auth()->user()->notifications->where("type","!=","App\Notifications\UserRequestedNotification");
        $notifications->markAsRead();
        return view("Notifications", compact("notifications"));
    }
}
