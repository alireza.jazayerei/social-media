<?php

namespace App\Http\Controllers;

use App\Notifications\UserRequestedNotification;
use App\Providers\RequestAccepted;
use App\User;
use Illuminate\Http\Request;

class RequestController extends Controller
{
    public function index()
    {
        $notifications = auth()->user()->notifications->where("type", "App\Notifications\UserRequestedNotification");
        $notifications->markAsRead();
        return view("Notifications", compact("notifications"));
    }

    public function store(User $user)
    {
        auth()->user()->requests()->toggle([
            "user_id" => $user->id,
        ]);
        auth()->user()->followers()->toggle([
            "user_id" => $user->id,
        ]);

        event(new RequestAccepted($user,auth()->user()));

        return back();
    }

    public function destroy(user $user)
    {
        auth()->user()->requests()->toggle([
            "user_id" => $user->id,
        ]);

        auth()->user()->unreadNotifications()->where("type",UserRequestedNotification::class)->delete();

        return back();
    }
}
