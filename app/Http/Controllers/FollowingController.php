<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class FollowingController extends Controller
{
    public function index()
    {
        $followers = auth()->user()->followings->load("media");
        return view("Followers", compact("followers"));
    }

    public function show(User $user)
    {
        $this->authorize("view",[$user,auth()->user()->isFollowing($user)]);
        $followers=$user->followers->load("media");
        return view("Followers", compact("followers"));
    }
}
