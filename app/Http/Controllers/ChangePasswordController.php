<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function show()
    {
        return view("ChangePassword");
    }

    public function store(ChangePasswordRequest $request)
    {
        if (!Hash::check($request->input("oldPassword"), auth()->user()->password)) {
            return back()->with(["passwordError" => "password is incorrect"]);
        }
        auth()->user()->password = Hash::make($request->input("newPassword"));
        auth()->user()->save();
        return redirect(route("profile.index"));
    }
}
