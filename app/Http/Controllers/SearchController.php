<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index()
    {
        $users = User::where("name", "like", "%" . request("search") . "%")->orwhere("username", "like", "%" . request("search") . "%")->get();
        return view("SearchResult",compact("users"));
    }
}
