<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Providers\CommentLiked;
use Illuminate\Http\Request;

class CommentLikeController extends Controller
{
    public function store(Comment $comment)
    {
        $this->authorize("view", [$comment->post, auth()->user()->isFollowing($comment->post->user)]);
        auth()->user()->likesComment()->toggle([
            "likable_id" => $comment->id,
        ]);

        event(new CommentLiked(auth()->user(), $comment));

        return back();
    }

    public function destroy(Comment $comment)
    {
        $this->authorize("view", [$comment->post, auth()->user()->isFollowing($comment->post->user)]);
        auth()->user()->likesComment()->toggle([
            "likable_id" => $comment->id,
        ]);

        $comment->decrement("likes_count");

        return back();
    }
}
