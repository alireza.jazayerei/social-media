<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Http\Requests\PostUpdateRequest;
use App\Media;
use App\Post;
use App\Providers\PostDeleted;
use App\Repository\UserRepository;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function create()
    {
        return view("Upload");
    }

    public function show(Post $post)
    {
        $this->authorize("view", [$post->user, auth()->user()->isFollowing($post->user)]);
        $hasLikedPost = (bool)auth()->user()->hasLikedPost([$post->id]);
        $likedComments = auth()->user()->likesComment->pluck("id");
        return view("SinglePost", compact("post", "hasLikedPost", "likedComments"));
    }

    public function store(PostRequest $request)
    {
        $mediaId = Media::firstOrUpload($request->file("image"));

        auth()->user()->posts()->create([
            "media_id" => $mediaId,
            "caption" => $request->input("caption"),
            "filter" => $request->input("filter") ?? NULL,
        ]);

        auth()->user()->increment("posts_count");
        return redirect(route("home"));
    }

    public function edit(Post $post)
    {
        $this->authorize("update", $post);

        return view("PostEdit", compact("post"));
    }

    public function update(Post $post, PostUpdateRequest $request, Media $media)
    {
        $this->authorize("update", $post);
        $attributes = $request->all();
        $attributes["media_id"] = $request->has("image") ? Media::firstOrUpload($request->file("image")) : $post->media->id;
        $post->update($attributes);
        return redirect(route("profile.index"));
    }

    public function destroy(Post $post)
    {
        $this->authorize("update", $post);
        event(new PostDeleted($post));
        $post->delete();
        auth()->user()->decrement("posts_count");
        return back();
    }
}
