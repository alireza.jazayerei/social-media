<?php

namespace App\Http\Controllers;

use App\Post;
use App\Providers\PostLiked;
use Illuminate\Http\Request;

class PostLikeController extends Controller
{
    public function store(post $post)
    {
        $this->authorize("view", [$post, auth()->user()->isFollowing($post->user)]);
        auth()->user()->likesPost()->toggle([
            "likable_id" => $post->id,
        ]);

        event(new PostLiked(auth()->user(), $post));

        return back();
    }

    public function destroy(post $post)
    {
        $this->authorize("view", [$post, auth()->user()->isFollowing($post->user)]);
        auth()->user()->likesPost()->toggle([
            "likable_id" => $post->id,
        ]);

        $post->decrement("likes_count");

        return back();
    }
}
