<?php

namespace App\Http\Controllers;

use App\Repository\UserRepository;

class HomeController extends Controller
{
    public function index()
    {
        $posts = auth()->user()->homePosts();
        $likedPosts = auth()->user()->likedPosts();
        $likedComments = auth()->user()->likedComments();

        return view('home', compact("posts", "likedPosts", "likedComments"));
    }
}
