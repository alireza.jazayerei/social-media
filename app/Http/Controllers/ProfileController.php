<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Media;
use App\Repository\UserRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function index()
    {
        $user = auth()->user()->load(["posts.media", "media"]);
        $likedPosts = auth()->user()->hasLikedPost(auth()->user()->posts()->pluck("id"));
        $likedComments = auth()->user()->hasLikedComment(auth()->user()->comments()->pluck("id"));
        return view("Profile", compact("user", "likedPosts", "likedComments"));
    }


    public function show(User $user)
    {
        if (auth()->user()->is($user)) {
            return redirect(route("profile.index"));
        }
        $user = $user->load(["posts.media"]);
        $followingStatus = auth()->user()->isFollowing($user);
        $requestedStatus = auth()->user()->hasRequested($user);
        $likedPosts = auth()->user()->hasLikedPost($user->posts()->pluck("id"));
        $likedComments = auth()->user()->hasLikedComment($user->comments()->pluck("id"));
        return view("People", compact("user", "followingStatus", "likedPosts", "likedComments", "requestedStatus"));
    }

    public function update(ProfileRequest $request)
    {
        auth()->user()->update([
            "media_id" => $request->has("image") ? Media::firstOrUpload($request->file("image")) : auth()->user()->media_id,
            "name" => $request->input("name"),
            "bio" => $request->input("bio"),
            "website" => $request->input("website"),
            "is_private" => $request->has("is_private"),
        ]);

        return back();
    }
}
