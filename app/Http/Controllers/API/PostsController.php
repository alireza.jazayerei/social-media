<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Http\Resources\PostCollection;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostsController extends Controller
{
    /**
     * Update the authenticated user's API token.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $followingId = auth()->user()->followings->pluck("id");
        $posts = Post::whereIn("user_id", $followingId)->with(["user.media", "media", "comments.user.media"])->get();
        return  PostCollection::collection($posts);

    }
}
