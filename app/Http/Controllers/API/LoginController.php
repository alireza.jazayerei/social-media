<?php


namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Validator;

class LoginController
{


    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            Auth::user();
            $token = Str::random(80);

            auth()->user()->forceFill([
                'api_token' => hash('sha256', $token),
            ])->save();

            return ['token' => $token];
        }
        return response()->json(['error' => 'Unauthorised'], 401);

    }

}

