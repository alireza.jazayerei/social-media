<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CommentCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "body" => $this->body,
            "comment_owner_media" => $this->user->media->src,
            "comment_owner_username" => $this->user->username,
            "likes_count" => $this->likes_count,
            "created_at" => $this->created_at->diffForHumans(),
        ];
    }
}
