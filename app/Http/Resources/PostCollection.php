<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PostCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "post_media" => $this->media->src,
            "post_owner_media" => $this->user->media->src,
            "post_owner_username" => $this->user->username,
            "comments" => $this->whenLoaded("comments", function () {
                return CommentCollection::collection($this->comments);
            }),
            "likes_count" => $this->likes_count,
            "comments_count" => $this->comments_count,
            "created_at" => $this->created_at->diffForHumans(),
        ];
    }
}
