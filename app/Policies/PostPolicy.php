<?php

namespace App\Policies;

use App\Post;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Post $post,$followingStatus)
    {
        return ! $post->user->is_private || $followingStatus || $user->is($post->user);
    }

    public function update(User $user,Post $post)
    {
        return $user->is($post->user);
    }
}
