<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProfilePolicy
{
    use HandlesAuthorization;

    public function view(User $user, User $requestedUser, $followingStatus)
    {
        return !$requestedUser->is_private || $followingStatus || $user->is($requestedUser);
    }
}
