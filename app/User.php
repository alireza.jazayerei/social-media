<?php

namespace App;

use App\Repository\UserRepository;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, UserRepository;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', "username", "media_id", "bio", "website", "is_private",
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        "is_private" => 'boolean',
    ];

    public function media()
    {
        return $this->belongsTo(Media::class)->withDefault([
            "src" => "profile.png",
        ]);
    }

    public function posts()
    {
        return $this->hasMany(Post::class)->latest();
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'follows', 'following_id', 'user_id')->withTimestamps();
    }

    public function followings()
    {
        return $this->belongsToMany(User::class, 'follows', 'user_id', 'following_id')->withTimestamps();
    }

    public function likesPost()
    {
        return $this->morphedByMany(Post::class, "likable")->withTimestamps();
    }

    public function likesComment()
    {
        return $this->morphedByMany(Comment::class, "likable")->withTimestamps();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function requests()
    {
        return $this->belongsToMany(User::class, 'follow_requests', 'requesting_id', 'user_id')->withTimestamps();
    }

    public function getRouteKeyName()
    {
        return 'username';
    }


    public function requesting()
    {
        return $this->belongsToMany(User::class, 'follow_requests', 'user_id', 'requesting_id')->withTimestamps();
    }

}
