<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/profile', "ProfileController@index")->middleware("auth")->name("profile.index");
Route::get('/profile/{user}', "ProfileController@show")->middleware("auth")->name("profile.show");
Route::patch('/profile', "ProfileController@update")->middleware("auth")->name("profile.update");

Route::get('posts/create', "PostController@create")->middleware("auth")->name("post.create");
Route::get('posts/{post}', "PostController@show")->middleware("auth")->name("post.show");
Route::post('posts/store', "PostController@store")->middleware("auth")->name("post.store");
Route::get('posts/{post}/edit', "PostController@edit")->middleware("auth")->name("post.edit");
Route::patch('posts/{post}', "PostController@update")->middleware("auth")->name("post.update");
Route::delete('posts/{post}', "PostController@destroy")->middleware("auth")->name("post.destroy");
//user/{user}/following
Route::post("/user/{user}/follow", "FollowController@store")->middleware("auth")->name("follow.store");
Route::delete("/user/{user}/follow", "FollowController@destroy")->middleware("auth")->name("follow.destroy");

Route::get("/followings", "FollowingController@index")->middleware("auth")->name("following.index");
Route::get("/user/{user}/followings", "FollowingController@show")->middleware("auth")->name("following.show");

Route::get("/followers", "FollowerController@index")->middleware("auth")->name("follower.index");
Route::get("/user/{user}/followers", "FollowerController@show")->middleware("auth")->name("follower.show");

Route::post("/posts/{post}/like", "PostLikeController@store")->middleware("auth")->name("like.store");
Route::delete("/posts/{post}/like", "PostLikeController@destroy")->middleware("auth")->name("like.destroy");

Route::post("/posts/{post}/comment", "CommentController@store")->middleware("auth")->name("comment.store");

Route::post("/comments/{comment}/like", "CommentLikeController@store")->middleware("auth")->name("comment.like.store");
Route::delete("/comments/{comment}/like", "CommentLikeController@destroy")->middleware("auth")->name("comment.like.destroy");

Route::get("/changePassword", "ChangePasswordController@show")->middleware("auth")->name("password.change.show");
Route::post("/changePassword", "ChangePasswordController@store")->middleware("auth")->name("password.change.store");

Route::get("/notifications", "NotificationController@index")->middleware("auth")->name("notifications.index");

Route::get("/requests", "RequestController@index")->middleware("auth")->name("requests.index");
Route::post("/requests/{user}", "RequestController@store")->middleware("auth")->name("requests.store");
Route::delete("/requests/{user}", "RequestController@destroy")->middleware("auth")->name("requests.destroy");

Route::get('/', 'HomeController@index')->middleware("auth")->name('home');

Route::get("/search", "SearchController@index")->middleware("auth")->name("search.index");

Route::get('login/google', 'Auth\LoginController@redirectToProvider');
Route::get('login/google/callback', 'Auth\LoginController@handleProviderCallback');

Auth::routes();

