<?php

use App\Http\Controllers\API\LoginController;
use App\Http\Controllers\API\PostsController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post("/login", [LoginController::class, "login"]);
Route::get("/posts", [PostsController::class, "index"])->middleware("auth:api");
